'use strict';
var gpio = require("pi-gpio");

var lightsIsOn = false;
var infinite = false;
var timerIsSet = false;

var outputPins = {
    k1: 11,
    k2: 12,
    k3: 13,
    k4: 15,
};

var inputPins = {
    y1: 16, //Gate switch
    y2: 18, //Pir sensor
    y3: 22  //Switch
};

var switchTimes = {
    start: 0,
    end: 0,
}

var pinStates = {
    on: 0,
    off: 1,
}

var timerFunction = function(){}

function invalidateTimer(){
    timerFunction = function(){}
    timerIsSet = false;
    infinite = false;
}

function setTimer(seconds){
    timerIsSet = true;
    setTimeout(function(){
        console.log("Execute timer");
        toggleLights(pinStates.off, true);
        invalidateTimer();
    },(seconds * 1000))
}

function setTimedLights(){
    if(!infinite && !lightsIsOn){
        toggleLights();
        setTimer((45*60));
    }
}

function toggleLights(state, force){
    if(!timerIsSet || force){
        let onOff = state !== undefined ? state : lightsIsOn ? pinStates.off : pinStates.on
        
        gpio.write(outputPins.k1,onOff, function(){});
        gpio.write(outputPins.k2,onOff, function(){});
        gpio.write(outputPins.k3,onOff, function(){});
        gpio.write(outputPins.k4,onOff, function(){});
        lightsIsOn = !lightsIsOn;

        var lightsOn = lightsIsOn ? "YES" : "NO"
        console.log("Lights: "+lightsOn);
    }
}

function gateSwitchInputDidChange(value){
    if(value == pinStates.off){
        setTimedLights();
    }
}

function pirSensorInputDidChange(value){
    if(value == pinStates.off){
        setTimedLights();
    }
}

function switchInputDidChange(value){
    if(value == pinStates.off){
        switchTimes.start = new Date();
    }else{
        switchTimes.end = new Date;

        var timedifference = (switchTimes.end - switchTimes.start);
        console.log("Timedifference: "+timedifference);

        if(timedifference >= 3*1000){
            infinite = !infinite;
            if(lightsIsOn){
                invalidateTimer();
            }
            toggleLights(undefined,true);
        }else {
            setTimedLights();
        }
    }
}

function readInputChange(){
    var y1PreValue = 0;
    var y2PreValue = 0;
    var y3PreValue = 0;

    console.log("START READING, default values = y1:"+y1PreValue+" y2:"+y2PreValue+" y3:"+y3PreValue);
    setInterval(function(){
        gpio.read(inputPins.y1,function(err, value){
            if(err) { return; }
            if(value !== y1PreValue){
                console.log("READ y1 have changed pre: "+y1PreValue+" new: "+value);
                y1PreValue = value;
                gateSwitchInputDidChange(value);
            }
        });
        gpio.read(inputPins.y2,function(err, value){
            if(err) { return; }
            if(value !== y2PreValue){
                console.log("READ y2 have changed pre: "+y2PreValue+" new: "+value);
                y2PreValue = value;
                pirSensorInputDidChange(value);
            }            
        });
        gpio.read(inputPins.y3,function(err, value){
            if(err) { return; }
            if(value !== y3PreValue){
                console.log("READ y3 have changed pre: "+y3PreValue+" new: "+value);
                y3PreValue = value;
                switchInputDidChange(value);
            }
        });
    },100);
}

function initializePins(){
    gpio.close(outputPins.k1,function(err){});
    gpio.close(outputPins.k2,function(err){});
    gpio.close(outputPins.k3,function(err){});
    gpio.close(outputPins.k4,function(err){});
    gpio.close(inputPins.y1,function(err){});
    gpio.close(inputPins.y2,function(err){});
    gpio.close(inputPins.y3,function(err){});

    gpio.open(outputPins.k1,"output",function(err){});
    gpio.open(outputPins.k2,"output",function(err){});
    gpio.open(outputPins.k3,"output",function(err){});
    gpio.open(outputPins.k4,"output",function(err){});
    gpio.open(inputPins.y1,"input",function(err){});
    gpio.open(inputPins.y2,"input",function(err){});
    gpio.open(inputPins.y3,"input",function(err){});

    gpio.write(outputPins.k1,pinStates.off,function(){
        console.log("INIT pin 11 write off");
    });
    gpio.write(outputPins.k2,pinStates.off,function(){
        console.log("INIT pin 12 write off");
    });
    gpio.write(outputPins.k3,pinStates.off,function(){
        console.log("INIT pin 13 write off");
    });
    gpio.write(outputPins.k4,pinStates.off,function(){
        console.log("INIT pin 15 write off");
    });
    console.log("Initialization COMPLETE");
}

initializePins();
readInputChange();